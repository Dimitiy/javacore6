package areacalcerv1;

import java.util.Scanner;

public class AreaCalcerv1 {

    
    public static void main(String[] args) {
       
        System.out.println("Введите радиус круга:");
        Scanner in=new Scanner(System.in);
        String r = in.nextLine();
        //BufferedReader
        System.out.print("Площадь круга равна ");
        System.out.println(_area(Integer.parseInt(r)));
    }
    public static double _area(int r){
        return Math.pow(r, 2)*Math.PI;
    }
}
