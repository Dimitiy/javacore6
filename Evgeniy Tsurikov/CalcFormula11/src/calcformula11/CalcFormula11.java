/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calcformula11;

import java.util.Scanner;

/**
 *
 * @author Евгений
 */
public class CalcFormula11 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        double s, a, b, alpha;
        Scanner in = new Scanner(System.in);
        System.out.println("Insert A B: ");
        a=in.nextDouble();
        b=in.nextDouble();
        System.out.println("Insert alpha: ");
        alpha=in.nextDouble();
        s=a*b*Math.sin(Math.toRadians(alpha))/2;
        System.out.printf("%.2f\n", s);
    }
    
}
