/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calcformula8;

import java.util.Scanner;

/**
 *
 * @author Евгений
 */
public class CalcFormula8 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        double s, p, r;
        Scanner in = new Scanner(System.in);
        System.out.println("Insert r: ");
        r=in.nextDouble();
        System.out.println("Insert p: ");
        p=in.nextDouble();
        s=p*r;
        System.out.printf("%.2f\n", s);
    }
    
}
