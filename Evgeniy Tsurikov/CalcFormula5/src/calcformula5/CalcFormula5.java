/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calcformula5;

import java.util.Scanner;

/**
 *
 * @author Евгений
 */
public class CalcFormula5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        double s,a;
        double alpha;
        Scanner in = new Scanner(System.in);
        System.out.println("Insert a: ");
        a=in.nextDouble();
        System.out.println("Insert alpha: ");
        alpha=in.nextDouble();
        s=Math.sin(Math.toRadians(alpha))*Math.pow(a, 2);
        System.out.printf("%.2f\n", s);
    }
    
}
