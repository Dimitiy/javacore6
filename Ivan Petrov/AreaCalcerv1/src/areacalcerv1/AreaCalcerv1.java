package areacalcerv1;
import java.io.*;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AreaCalcerv1 {


    public static void main(String[] args) {
    System.out.println("Введите радиус круга, введите целое число");
    double r = 3;
    //вариант 1 = заготовка для файлов (файл или консоль)
    //стиль Java SE Java Core
    
    Scanner in = new Scanner(System.in);
    r = in.nextDouble();
    
    if (  r <= 0  )
    {
        System.out.println("введено отрицательное. Таких радиусов не бывает.");
        return;
    }
    
    //вариант 2 = "сырые" данные, так приходят данные от счетчиков и иных устройств
    //стиль Java ME
    /*
    byte[]bytes = new byte[200];
        try {
            int count = System.in.read(bytes);
        } catch (IOException ex) {
            Logger.getLogger(AreaCalcerv1.class.getName()).log(Level.SEVERE, null, ex);
        }
    r = Double.parseDouble(new String(bytes));
    */
    
    double area = Math.PI * r * r;
    System.out.println("Площадь круга равна "+area);
        
        
    }
    
}
