
package check;
import java.util.Scanner;

public class Check {

 public static double readD(String value) {
        System.out.println(" Enter value " + value);
        Scanner scan = new Scanner(System.in);
        try {
            return scan.nextDouble();
        } catch (Exception e) {
            System.out.println("it not number");
            return readD(value);
        }
    }
    public static double readDPositive(String value){
        double v = readD(value);
        if (v <= 0) {
            System.out.println(" value " + value + " negative, it wrong" );
            return readDPositive(value);
        }
        else {
            return v;
        }
    }
}
