package sample.box;

public class CurTreaD {
    public static void main(String[] args) {
        Thread t = new Thread();
        System.out.println("Cur thread:" + t);
        t.setName("my Thread");
        System.out.println("After change name:"+t);
        try{
            for (int i = 5; i > 0; i--) {
                System.out.println(i);
                Thread.sleep(1000);
            }
        }catch (InterruptedException e){
            System.out.println("main Thread blocked");
        }
    }
}
