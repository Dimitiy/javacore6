package sample.box;

public class ThrowDemo {
    public static void main(String[] args) {
        try {
            demoproc();
        } catch (NullPointerException e) {
            System.out.println("Again catch: " + e);
        }
    }

    static void demoproc() {
        try {
            throw new NullPointerException("demo test");
        } catch (NullPointerException e) {
            System.out.println("In demoproc: " + e.getMessage());
            throw e;
        }
    }
}