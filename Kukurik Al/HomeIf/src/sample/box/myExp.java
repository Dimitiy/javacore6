package sample.box;

public class myExp extends Exception {
    private int detail;

    myExp(int a) {
        detail = a;
    }

    public String toString() {
        return "myExp[" + detail + "]";
    }
}

class ExepD {
    static void compute(int a) throws myExp {
        System.out.println(" Caled compute(" + a + ")");
        if (a > 10)
            throw new myExp(a);
        System.out.println(" Normal ending");
    }

    public static void main(String[] args){
        try {
            compute(1);
            compute(20);
        } catch (myExp e) {
            System.out.println("Get e :" + e);
        }
    }
}
