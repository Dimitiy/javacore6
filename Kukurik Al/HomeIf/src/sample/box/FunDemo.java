package sample.box;

public class FunDemo {
    static void procA() {
        try {
            System.out.println("Inside");
            throw new RuntimeException("demo");
        } finally {
            System.out.println("in Finally");
        }
    }


    public static void main(String[] args) {
        try {
            procA();
        } catch (Exception e) {
            System.out.println("exepted"+e );
        }
    }
}