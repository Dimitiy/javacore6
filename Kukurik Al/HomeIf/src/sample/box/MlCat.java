package sample.box;

public class MlCat {
    public static void main(String[] args) {
        try {
            int a = args.length;
            System.out.println("a= " + a);
            int b = 42/a;
            int c[]= {1};
            c[2]= 11;
        }
        catch (ArithmeticException e) {
            System.out.println("Division for zero" + e);
        }
        catch ( ArrayIndexOutOfBoundsException e){
            System.out.println("Wrong mass" + e);
        }
        System.out.println("after try & catch");
    }
}
