package sample.box;

public class NewTreads implements Runnable {

    Thread t;

    NewTreads() {
        t = new Thread(this, "Demo");
        System.out.println("Demonstrate created" + t);
        t.start();
    }
    
    @Override
    public void run(){
        try {
            for (int i = 5; i >0; i--) {
                System.out.println("Son of flow " +i);
                Thread.sleep(500);
            }
        }catch (InterruptedException e){
            System.out.println("Son of flow interrupt");
        }
        System.out.println("son of flow ended");
    }
}
class ThreadDemo{
    public static void main(String[] args) {
        new NewTreads();
        try {
            for (int i = 5; i > 0; i--) {
                System.out.println("main flow " + i);
                Thread.sleep(1000);
            }
        }catch (InterruptedException e) {
            System.out.println("Main flow interrupted");
        }
        System.out.println("main flow ended");

            }
        }

