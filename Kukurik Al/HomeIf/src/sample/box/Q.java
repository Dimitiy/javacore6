package sample.box;

public class Q {
    int n;

    synchronized int get() {
        System.out.println("Resived " + n);
        return n;
    }

    synchronized void put(int n) {
        this.n = n;
        System.out.println("Send " + n);
    }
}

class Producer implements Runnable {
    Q q;

    Producer(Q q) {
        this.q = q;
        new Thread(this, "Seller").start();
    }

    public void run() {
        int i = 0;
        while (true) {
            q.put(i++);
        }
    }
}

class Consum implements Runnable {
    Q q;

    Consum(Q q) {
        this.q = q;
        new Thread(this, "Byuer").start();
    }

    public void run() {
        while (true) {
            q.get();
        }
    }
}

class PS {
    public static void main(String[] args) {
        Q q = new Q();
        new Producer(q);
        new Consum(q);
    }
}