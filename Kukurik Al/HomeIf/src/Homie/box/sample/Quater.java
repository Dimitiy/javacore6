package Homie.box.sample;

public class Quater {
    public static void main(String[] args) {
        double x = Checkin.reaD("X");
        double y = Checkin.reaD("Y");

        if (x > 0 && y > 0) {
            System.out.println("Coordinate point in I coordinate quarter ");
        } else if (x > 0 && y < 0) {
            System.out.println("Coordinate point in II coordinate quarter ");
        } else if (x < 0 && y < 0) {
            System.out.println("Coordinate point in III coordinate quarter ");
        } else if (x < 0 && y > 0) {
            System.out.println("Coordinate point in IV coordinate quarter ");
        } else if (x == 0 || y == 0) {
            System.out.println("Coordinate point on border of quarters ");
        }
    }
}
