package Homie.box.sample;

import sample.box.Check;

public class CirkleTri {
    public void funct() {
        System.out.println("Radius of circle");
        double a = Check.readDPositive("a");
        System.out.println("side of triangle");
        double b = Check.readDPositive("b");
        double d = (a * Math.sqrt(3)) / 6;
        if (a <= d) {
            System.out.println("Circle in triangle");
        } else {
            System.out.println("Circle not in triangle");
        }
    }
}
class R7{
    public static void main(String[] args) {
        CirkleTri test = new CirkleTri();
        test.funct();
    }
}