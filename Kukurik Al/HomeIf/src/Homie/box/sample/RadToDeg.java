package Homie.box.sample;

public class RadToDeg {
    public double radToDeg() {
        double p = Checkin.reaD("rad");
        if (p <= 0) {
            System.out.println(" Input correct value ");
            return radToDeg();
        } else {
            double gard = p * (180 / Math.PI);
            return gard;
        }
    }
}

class Ru {
    public static void main(String[] args) {
        RadToDeg fu = new RadToDeg();
        for (int i = 0; i <5; i++) {
            System.out.println("Grad " + fu.radToDeg());
        }
    }
}
