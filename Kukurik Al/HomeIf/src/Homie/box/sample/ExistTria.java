package Homie.box.sample;

import sample.box.Check;

public class ExistTria {
    public void func() {
        double a = Check.readDPositive("a");
        double b = Check.readDPositive("b");
        double c = Check.readDPositive("c");
        if (a + b > c && a + c > b && b + c > a) {
            System.out.println("triangle exist");
        } else {
            System.out.println("triangle not exist");
        }
    }
}
class R16{
    public static void main(String[] args) {
        ExistTria test = new ExistTria();
        test.func();
    }
}
