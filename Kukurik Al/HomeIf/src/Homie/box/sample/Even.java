package Homie.box.sample;

public class Even {
    public static boolean even(String value) {
        double K = Checkin.reaD(value);
        if (K % 2 == 0) {
            System.out.println("K parity");
            return true;
        } else {
            System.out.println("K nonparity");
            return false;
        }
    }
}

class Realize {
    public static void main(String[] args) {
        int i = 0;
        while (i < 5) {
            if (Even.even("K")) {
                i++;
            }
            System.out.println(" quantity of parity " + i);
        }
    }
}
