
package cyclewhilefortests;


public class CycleWhileForTests {

    public static void main(String[] args) {

        // вывести элементы из интервала от 1 до 10 (первый десяток)
        int a = 1;
        int b = 10;
        System.out.println("решение с while");
        int step = 0 ; //номер шага
        int i = a;
        while (i <= b)
        {
            System.out.println("Номер шага цикла = "+step );
            System.out.println(i);
            step = step +1;
            i++; //i = i = 1;
        }
        System.out.println("решение с for");
        for( int j = a ; j <= b ; j = j + 1)
        {
            System.out.println(j);
        }
        // вывести на экран все четные числа из первого десятка
        //2  4 6 8
        // вывести на экран все нечетные числа из первого десятка
        // 11 13 15 17 19
        System.out.println(" вывести на экран все четные числа из первого десятка" );
        i = 0;
        while (i <=9)
        {
            // i = i + 2;
            System.out.println(i);
            i = i + 2;
        }
        System.out.println("Решение через for");
        for(i = 2 ; i <= 9 ; i = i + 2 )
        {
            System.out.println(i);
        }
//        System.out.println("вывести на экран все нечетные числа из второго десятка");
//        for (i = 11 ; i <= 19 ; i = i + 2 )
//        {
//            System.out.println(i);
//        }
        
//        for(i = 11; i <=19; i = i + 1) {
//            if(i % 2 != 0 )
//            {
//              System.out.println(i); 
//            }
//        }
        
    i = 11;
    while(i <= 19)
    {
        int m = i % 2;
        if (m == 1)
        {
            System.out.println(i);
        } 
        i = i + 1;
    }
    }

}
